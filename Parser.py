import numpy as np

#Load the data in csv format
#Each row contains an instance (case)
#The values included in each row are separated by a string given by the parameter sep, e.g., ","
#Each column corresponds to the values of a (discrete) random variable
#name (string): file name containing the data 
#sep (string): separates the different values of the data
def loadCsv(name,sep):
    text= np.loadtxt(name, np.str, delimiter=sep)
    n= text.shape[1]    
    data= np.column_stack([np.unique(text[:,i],return_inverse=True)[1] for i in range(n)])
    return data