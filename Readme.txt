This repository includes an implementation in Python of the Fractal Tree family of algorithms for learning decomposable models with a bounded treewidth.

The main properties of these algorithms can be summarized as follows:
    -Fractal trees deals with the problem of learning a decomposable model with a bounded clique size of k that maximizes the likelihood
    -Fractal trees have a computational complexity of O(k^2·n^2·N), in the worst case
    -Fractal Tree algorithms include a prune-and-graft operator that improves the quality of the obtained solution
    -All the provided procedures are modular and can be easily parallelized

See "A. Pérez, I. Inza and J.A. Lozano (2016). Efficient Approximation of Probability Distributions with $k$-order Decomposable Models. International Journal of Approximate Reasoning. Under Review." for further details

Content of the repository:

Program.py contains an example of the use of FractalTree algorithm.

FractalTree.py implements the main class of the FractalTree learning algorithms algorithm. It includes the Parallel Fractal Tree, Sequential Fractal Tree and the prune and graft procedure.

Separator.py implements the Separator class and the main operations that can be performed with the separators.