#########################################################################################
# An example of use of FractalTree family of algorithms                                 #
# Author: Aritz Perez (developer), Jose A. Lozano, Inaki Inza                           #
# Title: Efficient approximation of probability model with k-order decomposable models  #
# Journal: International Journal of Approximate Reasoning                               # 
# Year: 2016                                                                            #
#########################################################################################
import numpy as np #At least version 1.9
from Separator import Separator
from FractalTree import FractalTree
import Parser as ps
#import line_profiler as lp #The line profiler by Kernprof

def __main__():#k=5,fileName="mushroom.csv",seq=True,PG=True):
    #Learn an M5DG
    k=5 
    #Use the sequential fractal tree learning algorithm
    seq=True
    #Use the prune and graft procedure at the end of each growing step  
    PG=True 
    #Learn a model for the data set "data.csv" 
    fileName="data.csv" 
    #Load the data
    D=ps.loadCsv(fileName,",")
    #Initialize the model
    FT= FractalTree(D)
    #Learn the model
    if(seq):
        M= FT.learnSequential(k,PG)
    else:
        M= FT.learnParallel(k,PG)
    #Show the learned cliques
    print("The list of cliques")
    print(FT.getCliques())
    print("The adjacency matrix")
    print(FT.getAdjacenceMatrix())
    print("The list of separators and their mantles")
    print(FT.getSepsAndMantles())
    
if __name__ == '__main__':
    __main__()
