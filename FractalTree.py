#########################################################################################
# The FractalTree class                                                                 #
# Author: Aritz Perez (developer), Jose A. Lozano, Inaki Inza                           #
# Title: Efficient approximation of probability model with k-order decomposable models  #
# Journal: International Journal of Approximate Reasoning                               # 
# Year: 2016                                                                            #
#########################################################################################

import math
import numpy as np #from version 1.9
from Separator import Separator
#import line_profiler as lp #The line profiler by Kernprof

#_seps: separator (set of integers)
#_V: vertices of the mantle (set of integers)
#_index: edges (list of sets of pair of integers (Array of array[2]
#_C: connected components (Array of sets of integers)
#_nb: neighbor separators (list of separators)
class FractalTree:
    
    #Constructor: 
    #D: data (numpy.array(numpy.int64): discrete data with values {0,...,r_i} where r_i is the cardinality of variable X_i)
    def __init__(self, D):
        
        #Data
        self._D= D
        #Number of (discrete) random variables
        self._n= D.shape[1]
        #The cardinality of the random variables (number of distinct values from 0 to card-1)
        self._card= np.max(D, axis=0)+1
        #Number of instances (cases)
        self._N= D.shape[0]
        
    #Parallel Fractal Tree procedure
    # k (int): Learn an MkDG
    # PG (boolean): Apply the P&G procedure    
    #@profile
    def learnParallel(self, k, PG):
        self._k= k
        #Initialization of the first grow step
        S= Separator(np.array([],dtype=int),range(self._n))
        self._seps= [S]
        
        for i in range(k-1):
            #Parallel growing 
            self._seps= self.growParallel(self._seps)
            if i>0 and PG:
                #Apply prune and graft procedure
                self._seps= self.pruneAndGraft(self._seps)
                
        return self._seps
    
    #Parallel growing step 
    #@profile
    def growParallel(self, seps):
        #Solve each separator in parallel
        trees= list()
        for S in seps:
            if(len(S._C)>2):                    
                #Solve the separator problem (Prim's algorithm)
                trees.append(self.solveSeparator(S))
            else:
                #Separator problem is trivially solved
                trees.append([[S._C[0][0],S._C[1][0]]])
        
        #Add the obtained solutions
        for i in range(len(trees)):
            for e in trees[i]:
                seps[i].addEdgeAndPropragate(e)
                
        newSeps= self.createNextSeparators(seps)
        
        return newSeps
    
    #Sequential Fractal Tree procedure
    # k (int): Learn an MkDG
    # PG (boolean): Apply the P&G procedure
    #@profile
    def learnSequential(self, k, PG):
        self._k= k
        #Initialization of the first grow step
        S= Separator(np.array([],dtype=int),range(self._n))
        self._seps= [S]
        
        for i in range(k-1):
            #Solve each separator in parallel
            self._seps=self.growSequential(self._seps)
            
            if(i>0 & PG):
                #Apply prune and graft procedure
                self._seps= self.pruneAndGraft(self._seps)
                        
        return self._seps
    
    #Sequential growing step
    #@profile
    def growSequential(self,seps):
        
        #sort the separators of the input MiDG
        order= self.sortingCriteria(seps)
        
        num= 0;
        for ind in order:
            #Solve the separator problem
            T= self.solveSeparator(self._seps[ind])
            #Add the solution to the set of separators
            for e in T:
                seps[ind].addEdgeAndPropragate(e)
                
        #Obtain the separators of the M(I+1)DG, G_i+1
        newSeps= self.getNextSepList(seps)
        
        return newSeps
        
    #The sorting criteria used that defines the order in which the separators are solved by the sequential growing step
    def sortingCriteria(self, seps):
        #The length of the mantle multiplied by the cliques shared with other separators    
        return np.argsort([len(S._V)*len(S._A) for S in seps])
  

    #Prune and graft procedure
    #seps: the separators of an MiDG
    #@profile
    def pruneAndGraft(self, seps):
        #First apply prune and graft over the stems
        grafted= list()
        stems= [S for S in seps if len(S._A)==1]
                
        for S in stems:
            #Take the leaf vertices that have not been currently pruned and grafted
            leafs= [u for u in S._V if (not u in S._A) and (not u in grafted)]#[u for u in S._V if not np.all(np.in1d([u],S._A[0]._seps)) and u not in grafted]
            
            for u in leafs:
                i= np.argmax([self.computeGraftWeight(u,R._S) for R in seps])
                if not (seps[i]==S):
                    #prune
                    S.prune(u)
                    #graft
                    seps[i].graft(u)
                    #u can not be pruned and grafted again
                    grafted.append(u)

            #If it is no longer a separator
            if len(S._V)==1:
                #remove from the list of separators
                seps.remove(S)

                #remove from the neighborhood of its neighbors
                for l in S._adj:
                    for R in l:
                        v = np.setdiff1d(S._S,R._S, True)[0]
                        R._adj[R._A[v]].remove(S)
                        if len(R._adj[R._A[v]])==0:
                            for u in R._A:
                                #Actualize the indexes _A of the neighbor R
                                if(R._A[u]>R._A[v]):
                                    R._A[u]=R._A[u]-1
                            #Remove S from _A and from _adj
                            del R._adj[R._A[v]]
                            del R._A[v]
                     
                        #If the neighbor R is a stem append to to the list 
                        if len(R._adj)==1 and R not in stems:
                            stems.append(R)
                    
        #Finally apply prune and graft over the rest of separators
        rest= [S for S in seps if len(S._A)>1]
        for S in rest:
            #treat the leafs that have not been currently pruned and grafted
            leafs= [u for u in S._V if (not u in S._A) and (not u in grafted)] 
            for u in leafs:
                i= np.argmax([self.computeGraftWeight(u,R._S) for R in seps])
                if not (seps[i]==S):
                    #prune
                    S.prune(u)
                    #graft
                    seps[i].graft(u)
                    #Ensure that u is not pruned and grafted again
                    grafted.append(u)
                    
                    #If its mantle has less than two vertices loss its separator condition
                    if len(S._V)<2:#it is impossible: REMOVE this lines
                        seps.remove(S)
                        for R in S._A:
                            R._A.remove(S)
        return seps
    
    #Ok
    #@profile
    def solveSeparator(self,S):    
        #Compute the weights an select the best among different connected components in the mantle
        n= len(S._C)
        E= np.ndarray(shape=(n,n,2), dtype=int)
        W= np.zeros(shape=(n,n), dtype=float)
        for i in range(n-1):
            for j in range(i+1,n):
                wMax= -1
                uMax= -1
                vMax= -1
                for u in S._C[i]:
                    for v in S._C[j]:
                        w= self.computeWeight(u,v,S._S)
                        if(w> wMax):
                            wMax= w
                            uMax= u
                            vMax= v
                E[i,j]= [uMax,vMax]
                W[i,j]= wMax
                E[j,i]= [uMax,vMax]
                W[j,i]= wMax
                
        #Construct the maximum weighted spanning tree (Prim's algorithm with adjacency lists) coarser than the given forest
        unvisited= range(1,n)
        maxU= np.zeros(n)
        maxW= W[0,:]
        tree= np.ndarray(shape=(n-1,2), dtype=int)
        
        for l in range(n-1):
           ind= np.argmax(maxW[unvisited])
           v= unvisited[ind]
           u= maxU[v]
           tree[l,:]=E[u,v]
           del unvisited[ind]
           for w in unvisited:
               if(maxW[w]<W[v,w]):
                   maxW[w]= W[v,w]
                   maxU[w]=v
           
        return tree

    #Compute the weight associated to the addition of the edge (u,v) due to S, i.e., I(u;v|S)
    #@profile
    def computeWeight(self, u, v, S):
        return self.entropy(np.concatenate(([u],S))) + self.entropy(np.concatenate(([v],S))) - self.entropy(np.concatenate(([u,v],S)))- self.entropy(S)
    
    #Compute the weight associated to graft the vertex u in the separator S, i.e., I(u;S)
    #@profile
    def computeGraftWeight(self, u, S):
        return self.entropy([u])+ self.entropy(S)- self.entropy(np.concatenate(([u],S)))
    
    #Compute the entropy of a set of random variables
    #X ([int,...,int]: the indexes of the random variables in the data set _D.
    #@profile
    def entropy(self,X):
        
        if(len(X)==0):
            return 0
        
        cardX= self._card[X]
        n= len(cardX)
        N= np.shape(self._D)[0]
        numStates=np.prod(cardX)
        #Compute the entropy using numpy.unique (from Numpy vers. 1.9)
        w= np.array([1*np.product(cardX[(i+1):len(cardX)]) for i in range(len(cardX))],np.int64)
        ind= np.sum(np.multiply(self._D[:,X],w),axis=1)
        Nx= np.unique(ind,return_counts=True)[1]
        p= np.divide(Nx,np.float(N))
        return -np.sum(np.multiply(p,np.log2(p)))                 
    
    def getdataIndexes(self,D,indX,cardX):
        ind= x[indX[0]]
        for i in range(1,len(cardX)):
            ind= ind* card[i]+ x[indX[i]]
        return ind
    
    #@profile
    def getNextSepList(self,seps):

        #The next separators
        newSeps= list()      
        #indices of the next separators in adj list()
        sepDict= dict()
        
        #Create the new separators
        for S in seps:
            for u in S._V:
                if len(S._edges[S._index[u]])>1:
                    U= np.union1d(S._S, [u])#Create the separator
                    if not str(U) in sepDict:
                        sepDict.update({str(U):len(newSeps)})
                        newSeps.append(Separator(U,S._edges[S._index[u]]))
        
        #learn the adjacent separators
        for i in range(0,len(newSeps)-1):
            S= newSeps[i]
            for j in range(i+1,len(newSeps)):
                R= newSeps[j]
                u= np.setdiff1d(S._S,R._S, True)
                if(len(u)==1):
                    u=u[0]
                    if u in R._V:#S and R have a common clique and thus they are neighbors
                        v= np.setdiff1d(R._S,S._S, True)[0]
                        if v in S._A:
                            S._adj[S._A[v]].append(R)
                        else:
                            S._A.update({v:len(S._adj)})
                            S._adj.append([R])
                            
                        if u in R._A:
                            R._adj[R._A[u]].append(S)
                        else:
                            R._A.update({u:len(R._adj)})
                            R._adj.append([S])
                            
        return newSeps    
    
    
    #FUNCIONS TO SHOW THE LEARNED STRUCTURE
    
    #Get the list of maximal cliques
    def getCliques(self):
        C= {}
        for S in self._seps:
            for u in S._V:
                act= np.union1d(S._S, [u])
                key= np.str(act) 
                if(key not in C):
                    C.update({key:act})
        return np.row_stack([C[c] for c in C])
    
    #Get the list of separators and its common neighborhood
    def getSepsAndMantles(self):
        return [[S._S,S._V] for S in self._seps]
    
    #Get the adjacency matrix associated to the undirected graph represented by the separators and their mantles          
    def getAdjacenceMatrix(self):
        adjacency= np.zeros(shape=(self._n,self._n),dtype=int)
        cliques= self.getCliques()
        [r,c]= cliques.shape
        for ind in range(r):
            for i in range(c-1):
                for j in range(i+1,c):
                    adjacency[cliques[ind,i],cliques[ind,j]]=1
                    adjacency[cliques[ind,j],cliques[ind,i]]=1
        return adjacency
    