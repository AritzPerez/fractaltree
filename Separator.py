#########################################################################################
# The separator class                                                                   #
# Author: Aritz Perez (developer), Jose A. Lozano, Inaki Inza                           #
# Title: Efficient approximation of probability model with k-order decomposable models  #
# Journal: International Journal of Approximate Reasoning                               # 
# Year: 2016                                                                            #
#########################################################################################

import numpy as np #from version 1.9

#_S: separator (ordered ndarray)
#_V: vertices of the mantle (set of integers)
#_index: edges (list of sets of pair of integers (Array of array[2]
#_C: connected components (list of ndarray of sets of integers)
#_A: neighbor separators (list of separators)
class Separator:
    #Constructor: 
    #_S: separator; sorted np.array(vertex)
    #_V: vertices of the mantle; unsorted list(vertices)
    #_index: edges; dict(vertex: list(index in _V and in _edges))
    #_edges: edges; _edges[index]=list(vertex)
    #_C: connected components; list(list(vertex))
    #_A: adjacent separators; dict(vertex:index)
    #_adj: Adjacent separators; _adj[index]=list(Separator)
    def __init__(self, S, V):
        self._S= np.array(S)
        self._V= [u for u in V]
        self._index= dict({V[i]:i for i in range(len(V))})
        self._edges= [list() for u in V]
        self._C= [[u] for u in V]
        self._A= dict()
        self._adj= list()
        self_state= 0
    
    #Return the index of the connected component containing the vertex v
    def getComponentIndex(self, v):
         for index, item in enumerate(self._C):
            if v in item:
                return index
         return -1
     
    #Return the connected component containing the vertex v
    def getComponentContaining(self, v):
        for item in self._C:
            if v in item:
                return item   
        return []

    #Return all the vertices in the mantle of the separator 
    # not included in the connected component of the vertex v
    def getComponentComplement(self, v):
        complement= np.empty([1,0],dtype=int)
        for item in self._C:
            if not(v in item):
                complement= np.append(complement,item)
        
        return complement
    
    #Add the edge e to the mantle of the separator
    def addEdge(self,e):
        
        ind0= self.getComponentIndex(e[0])
        ind1= self.getComponentIndex(e[1])
        if(ind1!=ind0):
            self._edges[self._index[e[0]]].append(e[1])
            self._edges[self._index[e[1]]].append(e[0])
            self._C[ind0].extend(self._C[ind1])
            del self._C[ind1]
            return True
        
        return False
    
    #Propagate the changes produced by the addition of an edge to adjacent separators
    #Can change the mantle of adjacent separators 
    def addEdgeAndPropragate(self,e):
                
        if(self.addEdge(e)):
            [u,v]=e
            if u in self._A:
                for S in self._adj[self._A[u]]:                    
                    w= np.setdiff1d(self._S,S._S,True)[0]
                    
                    #add the vertex v to the mantle of S
                    S._index.update({v:len(S._V)})
                    S._V.append(v)

                    #add the edge (v,w) to S
                    S._edges[S._index[w]].append(v)      
                    S._edges.append([w])
                    S.getComponentContaining(w).append(v)
                                        
            if v in self._A:
                for S in self._adj[self._A[v]]:
                    w= np.setdiff1d(self._S,S._S,True)[0]
                    
                    #add the vertex u to the mantle of S
                    S._index.update({u:len(S._V)})
                    S._V.append(u)

                    #add the edge (u,w) to S
                    S._edges[S._index[w]].append(u)                    
                    S._edges.append([w])
                    S.getComponentContaining(w).append(u)
                    
            if((u in self._A) & (v in self._A)):
                for S in self._adj[self._A[u]]:
                    for R in self._adj[self._A[v]]:
                        if np.intersect1d(S._S,R._S, True).size == (S._S.size-1):
                            if(v in S._A):
                                S._adj[S._A[v]].append(R)
                            else:
                                S._A.update({v:len(S._adj)})
                                S._adj.append([R])
                            
                            if(u in R._A):
                                R._adj[R._A[u]].append(S)
                            else:
                                R._A.update({u:len(R._adj)})
                                R._adj.append([S])
                                
            return True         
        return False
    
    #Prunes the vertex u from the mantle of the separator
    def prune(self,u):
        ind= self._index[u]
        #remove from the common neighborhood            
        del self._V[ind]
        #remove the associated (empty) set of edges
        del self._edges[ind]
        #delete the index
        del self._index[u]
        #actualize indexes
        for i in range(ind,len(self._V)):
            self._index[self._V[i]]-=1

        #remove its connected component (it is composed by a single vertex)
        del self._C[ind]

    #Grafts the vertex u in the mantle of the separator
    def graft(self,u):
        self._V.append(u)
        self._index.update({u: len(self._V)-1})
        self._edges.append([])
        self._C.append([u])
        
    #OK
    def getCandidateEdges(self):
        return [[u,v] for i in range(len(self._C)-1) for j in range(i+1,len(self._C)) for u in self._C[i] for v in self._C[j]]

    def __eq__(self,S):
        return np.array_equiv(self._S, S._S)
    
    #DEBUGING PROCEDURES
    def checking(self):
        for u in self._V:
            if self.getComponentIndex(u)<0:
                return False
            if u not in self._index:
                return False
            if self._index[u]<0 or self._index[u]>= len(self._V):
                return False
            if self._V[self._index[u]]!=u:
                return False
            if u in self._S:
                return False
        for u in self._A:
            if u not in self._V:
                return False
            if u not in self._index:
                return False
        return True
    